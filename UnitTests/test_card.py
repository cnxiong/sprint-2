from django.test import TestCase
from unittest import TestCase
from AppATDD.classes import card

# TODO
#  TODO COMPLETED FOR SPRINT I PASSING 100%

class CardGetSuite(TestCase):
    def setUp(self):
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)
        self.c1 = card.CardClass(1, 1)
        self.c2 = card.CardClass(1, 2)
        self.c3 = card.CardClass(2, 4)
        self.c4 = card.CardClass(2, 5)
        self.c5 = card.CardClass(3, 7)
        self.c6 = card.CardClass(3, 8)
        self.c7 = card.CardClass(4, 10)
        self.c8 = card.CardClass(4, 11)

    # This test will be to test that the getSuit() method of a card will return an accurate value
    def test_GetSuitAccuracy(self):
        # check that, if the card's suit is x, its getSuit() method returns x
        self.assertEqual(self.c1.getSuit(), 1)
        self.assertEqual(self.c2.getSuit(), 1)
        self.assertEqual(self.c3.getSuit(), 2)
        self.assertEqual(self.c4.getSuit(), 2)
        self.assertEqual(self.c5.getSuit(), 3)
        self.assertEqual(self.c6.getSuit(), 3)
        self.assertEqual(self.c7.getSuit(), 4)
        self.assertEqual(self.c8.getSuit(), 4)

    # This test is to ensure that no suit is outside of the acceptable range
    def test_GetSuitRange(self):
        # test(s)
        self.assertLessEqual(self.c1.getSuit(), 4)  # This will be used to make sure any card drawn is within the
        self.assertGreaterEqual(self.c1.getSuit(), 1)  # acceptable range of suits (1-4).
        self.assertLessEqual(self.c3.getSuit(), 4)  # This will be used to make sure any card drawn is within the
        self.assertGreaterEqual(self.c3.getSuit(), 1)  # acceptable range of suits (1-4).
        self.assertLessEqual(self.c5.getSuit(), 4)  # This will be used to make sure any card drawn is within the
        self.assertGreaterEqual(self.c5.getSuit(), 1)  # acceptable range of suits (1-4).
        self.assertLessEqual(self.c7.getSuit(), 4)  # This will be used to make sure any card drawn is within the
        self.assertGreaterEqual(self.c7.getSuit(), 1)  # acceptable range of suits (1-4).

class CardGetRank(TestCase):
    def setUp(self):
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)
        self.c1 = card.CardClass(1, 1)
        self.c2 = card.CardClass(1, 2)
        self.c3 = card.CardClass(2, 4)
        self.c4 = card.CardClass(2, 5)
        self.c5 = card.CardClass(3, 7)
        self.c6 = card.CardClass(3, 8)
        self.c7 = card.CardClass(4, 10)
        self.c8 = card.CardClass(4, 11)

    # This test will be to test that the getRank() method of a card will return an accurate value
    def test_GetRankAccuracy(self):
        # test(s)
        self.assertEqual(self.c1.getRank(), 1)
        self.assertEqual(self.c2.getRank(), 2)
        self.assertEqual(self.c3.getRank(), 4)
        self.assertEqual(self.c4.getRank(), 5)
        self.assertEqual(self.c5.getRank(), 7)
        self.assertEqual(self.c6.getRank(), 8)
        self.assertEqual(self.c7.getRank(), 10)
        self.assertEqual(self.c8.getRank(), 11)

    def test_GetRankRange(self):
        # This will be used to make sure any card drawn is within the
        # acceptable range of ranks (1-13).
        self.assertLessEqual(self.c1.getRank(), 13)
        self.assertGreaterEqual(self.c1.getRank(), 1)
        self.assertLessEqual(self.c2.getRank(), 13)
        self.assertGreaterEqual(self.c2.getRank(), 1)
        self.assertLessEqual(self.c3.getRank(), 13)
        self.assertGreaterEqual(self.c3.getRank(), 1)
        self.assertLessEqual(self.c4.getRank(), 13)
        self.assertGreaterEqual(self.c4.getRank(), 1)
        self.assertLessEqual(self.c5.getRank(), 13)
        self.assertGreaterEqual(self.c5.getRank(), 1)
        self.assertLessEqual(self.c6.getRank(), 13)
        self.assertGreaterEqual(self.c6.getRank(), 1)
        self.assertLessEqual(self.c7.getRank(), 13)
        self.assertGreaterEqual(self.c7.getRank(), 1)
        self.assertLessEqual(self.c8.getRank(), 13)
        self.assertGreaterEqual(self.c8.getRank(), 1)

class CardGetValue(TestCase):
    def setUp(self):
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)
        self.c1 = card.CardClass(1, 1)
        self.c2 = card.CardClass(1, 2)
        self.c3 = card.CardClass(2, 4)
        self.c4 = card.CardClass(2, 5)
        self.c5 = card.CardClass(3, 7)
        self.c6 = card.CardClass(3, 8)
        self.c7 = card.CardClass(4, 10)
        self.c8 = card.CardClass(4, 11)

    # Initialize some Cards, and assign them a value and check that it worked
    def test_CardGetValue(self):
        # Testing that card value is initializing to current default value (0)
        self.assertEqual(0, self.c1.getValue())
        self.assertEqual(0, self.c2.getValue())
        self.assertEqual(0, self.c3.getValue())
        self.assertEqual(0, self.c4.getValue())
        self.assertEqual(0, self.c5.getValue())
        self.assertEqual(0, self.c6.getValue())
        self.assertEqual(0, self.c7.getValue())
        self.assertEqual(0, self.c8.getValue())

class CardInitRaiseValueError(TestCase):
    def test_RaiseValueErrorZero(self):  # test with parameters of zero

        with self.assertRaises(ValueError):
            fakeCard = card.CardClass(0, 1)  # zero suit
        with self.assertRaises(ValueError):
            fakeCard = card.CardClass(1, 0)  # testing here with 0's on both suit and rank returns a value error
        with self.assertRaises(ValueError):
            fakeCard = card.CardClass(-1, 0)
        with self.assertRaises(ValueError):
            fakeCard = card.CardClass(0, 1)
        with self.assertRaises(ValueError):
            fakeCard = card.CardClass(0, 0)
        with self.assertRaises(ValueError):
            fakeCard = card.CardClass(0, -1)

    def test_RaiseValueErrorNegative(self):                     #test with negative parameters
        with self.assertRaises(ValueError):
            card.CardClass(-1, 1)
        with self.assertRaises(ValueError):
            card.CardClass(1, -1)  # testing here that inputing negative values returns a value error
        with self.assertRaises(ValueError):
            card.CardClass(-1, 1)
        with self.assertRaises(ValueError):
            card.CardClass(-1, -1)

    def test_RaiseValueErrorOverLimit(self):                    #test with parameters that are above the max
        with self.assertRaises(ValueError):
            card.CardClass(6, 4)
        with self.assertRaises(ValueError):
            card.CardClass(4, 14)
        with self.assertRaises(ValueError):
            card.CardClass(5, 6)  # test that inputting suit and card parameters
        with self.assertRaises(ValueError):
            card.CardClass(3, 14)

    def test_RaiseTypeError(self):
        with self.assertRaises(TypeError):
            card.CardClass("thingy", 1)  # test with invalid type parameters for a TypeError
        with self.assertRaises(ValueError):
            card.CardClass(False, 1)
        with self.assertRaises(TypeError):
            card.CardClass(2, "think")
        with self.assertRaises(ValueError):
            card.CardClass(2, False)

class CardInitCorrectRankAndSuit(TestCase):
    def setUp(self):
        # Card(suit, rank)
        self.aceClubs = card.CardClass(1, 1)                         #creating multiple cards to test
        self.twoClubs = card.CardClass(1, 2)
        self.fourDiamonds = card.CardClass(4, 4)
        self.kingHearts = card.CardClass(3, 13)

    def test_GetRank(self):
        self.assertEqual(self.aceClubs.getRank(), 1)            #testing the rank is set correctly for each card
        self.assertEquals(self.twoClubs.getRank(), 2)
        self.assertEqual(self.fourDiamonds.getRank(), 4)
        self.assertEqual(self.kingHearts.getRank(), 13)

    def test_GetSuit(self):
        self.assertEqual(self.aceClubs.getSuit(), 1)            #testing the suit is set correctly for each card
        self.assertEqual(self.twoClubs.getSuit(), 1)
        self.assertEqual(self.fourDiamonds.getSuit(), 4)
        self.assertEqual(self.kingHearts.getSuit(), 3)

    def test_GetSuit(self):
        self.assertEqual(self.aceClubs.getSuit(), 1)            #testing the suit is set correctly for each card
        self.assertEqual(self.twoClubs.getSuit(), 1)
        self.assertEqual(self.fourDiamonds.getSuit(), 4)
        self.assertEqual(self.kingHearts.getSuit(), 3)

class CardStr(TestCase):
    # Card tests
    def setUp(self):
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)

        # Clubs
        self.aceClubs = card.CardClass(1, 1)
        self.nineClubs = card.CardClass(1, 9)
        self.twoClubs = card.CardClass(1, 2)
        # Spades
        self.threeSpades = card.CardClass(2, 3)
        self.tenSpades = card.CardClass(2, 10)
        self.fourSpades = card.CardClass(2, 4)
        # Hearts
        self.aceHearts = card.CardClass(3, 1)
        self.nineHearts = card.CardClass(3, 9)
        self.twoHearts = card.CardClass(3, 2)
        # Diamonds
        self.threeDiamonds = card.CardClass(4, 3)
        self.tenDiamonds = card.CardClass(4, 10)
        self.fourDiamonds = card.CardClass(4, 4)

    def test_Str(self):
        # intentional failures, until __str__ is implemented correctly in card.py
        # Because __str__ is unimplemented, it will always be blank, rather than the correct String.
        # Clubs
        self.assertEquals(self.aceClubs.__str__(), "AC")
        self.assertEquals(self.twoClubs.__str__(), "2C")
        self.assertEquals(self.nineClubs.__str__(), "9C")
        # Spades
        self.assertEquals(self.threeSpades.__str__(), "3S")
        self.assertEquals(self.tenSpades.__str__(), "10S")
        self.assertEquals(self.fourSpades.__str__(), "4S")
        # Hearts
        self.assertEquals(self.aceHearts.__str__(), "AH")
        self.assertEquals(self.nineHearts.__str__(), "9H")
        self.assertEquals(self.twoHearts.__str__(), "2H")
        # Diamonds
        self.assertEquals(self.threeDiamonds.__str__(), "3D")
        self.assertEquals(self.tenDiamonds.__str__(), "10D")
        self.assertEquals(self.fourDiamonds.__str__(), "4D")
