from django.test import TestCase
from unittest import TestCase
from AppATDD.classes import card
from AppATDD.classes import deck
from AppATDD.classes import hand

#  TODO COMPLETED FOR SPRINT I PASSING 100%

class HandTest(TestCase):
    def setUp(self):
        # Example hand with a starting bet of 0
        self.testDeck = deck.DeckClass()
        self.testDeck2 = deck.DeckClass()
        self.fakeHand = hand.HandClass(self.testDeck)
        self.fakeHand2 = hand.HandClass(self.testDeck2)
        self.fakeCard = card.CardClass(1,1)
        self.card1 = card.CardClass(1, 2)
        self.card2 = card.CardClass(1, 3)
        self.card3 = card.CardClass(1, 4)
        self.card4 = card.CardClass(1, 5)
        self.card5 = card.CardClass(1,6)
        self.cards =[self.card1,self.card2,self.card3,self.card4,self.card5]

    def test_constructor(self):
        pass
        # REMOVED BET PARAMETER FROM CLASS------
        # Making a bad constructor check that it raises an error
        # with self.assertRaises(ValueError):
            # hand.HandClass(-1,self.testDeck) #Ensuring negative starting bets are not allowed
        # with self.assertRaises(TypeError):
            # hand.HandClass('s',self.testDeck) #Ensuring invalid starting bets are not allowed

    def test_invalid_card_adding(self):
        #Ensuring that added cards have valid suits and ranks
        with self.assertRaises(ValueError):
            self.cards[0] = card.CardClass(-1,1)
            self.fakeHand.overwrite_hand(self.cards) #Negative suit
        with self.assertRaises(ValueError):
            self.cards[0] = card.CardClass(1, -1)
            self.fakeHand.overwrite_hand(self.cards) #Negative rank
        with self.assertRaises(TypeError):
            self.cards[0] = card.CardClass('s',1)
            self.fakeHand.overwrite_hand(self.cards) #Invalid suit
        with self.assertRaises(TypeError):
            self.cards[0] = card.CardClass(1,'r')
            self.fakeHand.overwrite_hand(self.cards) #Invalid rank

    def test_add_five_plus(self):
        #Ensuring that a hand cannot have more than 5 cards
        with self.assertRaises(ValueError):
            self.cards.append(card.CardClass(2,7))
            self.fakeHand.overwrite_hand(self.cards)

    def test_new_hand(self):
        #Ensuring that the initial size of the hand is 5
        self.assertEqual(len(self.fakeHand2.cards),5)

        # #Adding a card and assuring that the size of the hand is not 0
        self.fakeHand.overwrite_hand(self.cards)
        self.assertNotEqual(len(self.fakeHand.cards),0)
        self.assertEqual(len(self.fakeHand.cards), 5)

        #Calling empty_hand and ensuring the hand size is 0
        self.fakeHand.empty_hand()
        self.assertEqual(len(self.fakeHand.cards),0)

    def test_overwrite_hand(self):

        with self.assertRaises(ValueError):
            self.cards.append(card.CardClass(1, 1))
            self.fakeHand.empty_hand()
            self.fakeHand.overwrite_hand(self.cards)

        with self.assertRaises(ValueError):
            self.cards.append(card.CardClass(1, 2))
            self.fakeHand.empty_hand()
            self.fakeHand.overwrite_hand(self.cards)

        with self.assertRaises(ValueError):
            self.cards.append(card.CardClass(1, 2))
            self.fakeHand.empty_hand()
            self.fakeHand.overwrite_hand(self.cards)