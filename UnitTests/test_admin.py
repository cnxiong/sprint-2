from AppATDD.classes.account import AccountID
from AppATDD.classes.adminaccount import Admin
from AppATDD.classes.player import PlayerClass
from AppATDD.classes.table import TableClass
from unittest import TestCase

# TODO
#   WIP: Work in Progress SPRINT II IMPLEMENTATION... ~passing 30%

class AdminClass(TestCase):

    def setUp(self):
        admin = Admin(username="admin", password="password", is_online=True)

    def test_constructor(self):
        with self.assertRaises(NameError):
            Admin(username=None, password="password", is_online=True)
        with self.assertRaises(NameError):
            Admin(username="adminX", password=None, is_online=True)
        with self.assertRaises(TypeError):
            Admin(username=0, password="password", is_online=True)
        with self.assertRaises(TypeError):
            Admin(username="adminXX", password=0, is_online=True)
        with self.assertRaises(TypeError):
            Admin(username="adminXXX", password="password", is_online=0)

    def test_create_new_player(self, user_id, dealer_id):
        # SPRINT II Implemenation
        pass

    def test_login(self, dealer_login, dealer_password):
        # SPRINT II Implemenation
        pass