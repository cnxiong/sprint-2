from django.views import View

from AppATDD.classes.game import GameClass
from AppATDD.classes.table import TableClass
from AppATDD.models import Admin, Player, Table, Game
from django.shortcuts import render, redirect
from AppATDD.classes import adminaccount, game, player, table


# Create your views here.


class Home(View):
    def get(self, request):
        return render(request, 'main/home.html', {"Usernames": Player.objects.all()})

    def post(self, request):
        pass


class Login(View):
    message = ''

    def get(self, request):
        if request.session.get('username'):
            return redirect('user')

        return render(request, 'main/login.html')

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']

        isPlayer = Player.objects.filter(username=username)
        if password == 'password':
            if not isPlayer:
                message = 'Invalid player username please choose one on the board'
                return render(request, 'main/home.html', {'error_message': message,
                                                          "Usernames": Player.objects.all()})
            request.session["username"] = username
            return redirect('player')

        admin = Admin.objects.filter(username=username)
        if not admin:
            message = 'no such admin exist, please register first'
            return render(request, 'main/register.html', {'error_message': message})

        adminPassword = Admin.objects.get(username__exact=username)
        if adminPassword.password != password:
            message = 'invalid password, please try again'
            return render(request, 'main/login.html', {'error_message': message})

        request.session["username"] = username
        return redirect('table')


class Logout(View):
    def get(self, request):
        request.session.pop("username", None)
        return redirect('home')


class Register(View):
    message = ''

    def get(self, request):
        return render(request, 'main/register.html')

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        confirmationPassword = request.POST["confirm_password"]

        if password != confirmationPassword:
            message = 'password does not match please try again'
            return render(request, 'main/register.html', {'error_message': message})
        newUser = Admin.objects.filter(username=username)

        # TODO check if there the admin is already exist in the database
        if not newUser:
            Admin.objects.create(username=username, password=password, is_online=True)
            return redirect('login')
        # Todo if username already taken prompt for new username
        message = 'Username already taken'
        return render(request, 'main/register.html', {'error_message': message})


class TableView(View):
    message = ''

    def get(self, request):
        if not request.session.get('username'):
            message = 'Please login first before using this page'
            return render(request, 'main/login.html', {'error_message': message, 'Table': Table.objects.all()})

        username = request.session.get('username')
        isPlayer = Player.objects.filter(username=username)
        if isPlayer:
            return render(request, 'main/playerTable.html', {'Table': Table.objects.all()})

        count = 5

        players = Player.objects.all()
        topPlayers = []
        listPlayers = list(players)
        for p in players:
            i = 0
            for top in listPlayers:
                if p.highscore > top.highscore and count != 0:
                    topPlayers.append(p)
                    count = count - 1

        return render(request, 'main/table.html', {'Players': topPlayers,
                                                   'Admin': Admin.objects.all(),
                                                   'Table': Table.objects.all()})

    def post(self, request):
        message = ''
        tableName = request.POST.get('create_table', False)
        tableNameDelete = request.POST.get('delete_table', False)

        count = 5

        players = Player.objects.all()
        topPlayers = []
        listPlayers = list(players)
        for p in players:
            i = 0
            for top in listPlayers:
                if p.highscore > top.highscore and count != 0:
                    topPlayers.append(p)
                    count = count - 1

        if not tableNameDelete and tableName:

            newTable = Table.objects.filter(tablename=tableName)
            if newTable:
                message = 'Table already exist, please choose different name'
                return render(request, 'main/table.html', {'error_message': message,
                                                           'Table': Table.objects.all(),
                                                           'Players': topPlayers})

            Table.objects.create(tablename=tableName, seats_available=5, seats_taken=0)
            message = f'{tableName} is successfully created'
            return render(request, 'main/table.html', {'error_message': message,
                                                       'Table': Table.objects.all(),
                                                       'Players': topPlayers})

        elif tableNameDelete and not tableName:
            deleteTable = Table.objects.filter(tablename=tableNameDelete)
            if not deleteTable:
                message = f'{tableNameDelete} does not exist, please enter a valid table name'
                return render(request, 'main/table.html', {'error_message': message,
                                                           'Table': Table.objects.all(),
                                                           'Players': topPlayers})
            Table.objects.get(tablename__exact=tableNameDelete).delete()
            message = f'{tableNameDelete} have been deleted'
            return render(request, 'main/table.html', {'error_message': message,
                                                       'Table': Table.objects.all(),
                                                       'Players': topPlayers})
        return render(request, 'main/table.html', {'error_message': message,
                                                   'Table': Table.objects.all(),
                                                   'Players': topPlayers})


class User(View):
    message = ''

    def get(self, request):
        if not request.session.get('username'):
            message = 'Please login first before using this page'
            return render(request, 'main/login.html', {'error_message': message})
        username = request.session.get('username')
        isAdmin = Admin.objects.filter(username__exact=username)

        if isAdmin:
            message = 'Player user name does not exist'
            return render(request, 'main/user.html', {'Players': Player.objects.all(),
                                                      'error_message': message})

        return render(request, 'main/playerTable.html', {'Table': Table.objects.all()})

    def post(self, request):
        add = request.POST.get('add', False)
        delete = request.POST.get('delete', False)

        if add and not delete:
            playerName = Player.objects.filter(username__exact=add)
            if playerName:
                message = 'Player username already exist'
                return render(request, 'main/user.html', {'Players': Player.objects.all(),
                                                          'error_message': message})
            Player.objects.create(username=add, chips=100, amount_bet=0, highscore=0, status='NONE')
            message = f'Player {add} is added'
            return render(request, 'main/user.html', {'Players': Player.objects.all(),
                                                      'error_message': message})
        else:
            playerName = Player.objects.filter(username__exact=delete)
            if not playerName:
                message = 'Player username does not exist, choose an exist username'
                return render(request, 'main/user.html', {'Players': Player.objects.all(),
                                                          'error_message': message})
            Player.objects.filter(username__exact=delete).delete()
            message = f'Player {delete} is deleted'
            return render(request, 'main/user.html', {'Players': Player.objects.all(),
                                                      'error_message': message})

class PlayerView(View):
    def get(self, request):
        if request.session.get('username'):
            username = request.session.get('username');
            isPlayer = Player.objects.filter(username=username)
            if not isPlayer:
                message = 'this page is only for players'
                return render(request, 'main/home.html', {'Useranme': Player.objects.all(),
                                                          'error_message': message})
            return render(request, 'main/playerTable.html', {'Table': Table.objects.all()})

    def post(self, request):

        join = request.POST.get('join', False)

        if join:
            return render(request, 'main/game.html', {'Admin': Admin.objects.all(),
                                                      'Table': Table.objects.all()})
        return render(request, 'main/game.html', {'Table': Table.objects.all()})

class Game(View):
    message = ''

    def get(self, request):
        if not request.session.get('username'):
            message = 'Please login first before using this page'
            return render(request, 'main/login.html', {'error_message': message})
        return render(request, 'main/game.html')

    def post(self, request):
        choice = request.POST.get('playerChoice', False)
        amount = request.POST.get('amount', False)
        tableName = request.POST.get('table_name', False)
        player = request.POST.get('username', False)  # TODO the player is playing

        # MUST HAVE A TABLE MADE WITH PLAYERS AT THE TABLE
        table1 = TableClass([], tableName)
        game1 = GameClass(table1.table_players, table)
        game1.start()  # gives all players new hands

        count = 0
        # if the model already exists pass in a blank array it will repopulate players
        for playerX in game1.players_in_game:
            count = count + 1
            if playerX.username == player:
                if choice == "RAISE":
                    playerX.player_raise(amount, table1)
                if choice == "BET":
                    playerX.player_bet(amount, table1)
                if choice == "FOLD":
                    playerX.player_fold(amount, table1)
                if choice == "CHECK":
                    playerX.player_check(amount, table1)
                if choice == "CALL":
                    playerX.player_call(amount, table1)

            if count == 4:  # on the last player
                game1.game_over()

            return render(request, 'main/game.html', {'players': game1.players_in_game, 'game': game1})
