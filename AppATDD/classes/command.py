from AppATDD.models import Player
from AppATDD.models import Admin
from AppATDD.models import Table
from AppATDD.models import Game
from AppATDD.classes.player import PlayerClass
from AppATDD.classes.adminaccount import AdminClass
from AppATDD.classes.table import TableClass
from AppATDD.classes.game import GameClass


class Command:

    def command(self, args):
        input = args.split(":")
        first = input[0]
        print(input)

        if first == "demo":
            self.generate_class_demo()

        if first == "demo2":
            self.generate_class_demo2()

    def generate_class_demo(self):
        # (username, password, online):

        # try:
        #     admin = Admin.objects.get(username__exact="NewAdmin").delete()
        #     print("deleting existing admin")
        # except:
        #     print("creating new admin")

        admin = AdminClass("NewAdmin", "password", False)
        admin.go_online("NewAdmin", "password")
        admin.reset_password("newPassword")
        admin.check_username("NewAdmin")
        admin.set_username("NewAdmin2")
        admin.set_username("NewAdmin")

        # players not tied to deletion of admin right now...
        try:
            Player.objects.get(username__exact="player1").delete()
            Player.objects.get(username__exact="player2").delete()
            Player.objects.get(username__exact="player3").delete()
            print("deleting existing players")
        except:
            print("creating new players")

        player1 = PlayerClass("player1")
        player2 = PlayerClass("player2")
        player3 = PlayerClass("player3")
        player4 = PlayerClass("player4")
        player5 = PlayerClass("player5")

        # get model method is embedded into classes

        # players not tied to deletion of admin right now...
        try:
            Table.objects.get(tablename__exact="Table1").delete()
            Table.objects.get(tablename__exact="Table2").delete()
            print("deleting existing tables")
        except:
            print("creating new tables")

        players = [player1, player2, player3]
        table1 = TableClass(players, "Table1")
        players = [player4, player5]
        table2 = TableClass(players, "Table2")
        table1.add_player_to_table(player1)

        # TABLE 1 COMMANDS
        table1.remove_player(player1)
        table1.add_player_to_table(player1)
        table1.add_player_to_table(player2)
        table1.add_player_to_table(player3)

        # TABLE 2 COMMANDS
        table2.add_player_to_table(player4)  # will not cause error and will not add doubles :D
        table2.add_player_to_table(player4)  # will not cause error and will not add doubles :D
        table2.add_player_to_table(player5)

        try:
            Game.objects.get(tablename__exact="Table1").delete()
            Game.objects.get(tablename__exact="Table2").delete()
            print("deleting existing games")
        except:
            print("creating new games")

        game1 = GameClass(table1.table_players, "Table1")
        game2 = GameClass(table2.table_players, "Table2")

        game1.start()  # gives all players new hands
        game2.start()

        player1.player_buy_in(50,"Table1")
        player2.player_buy_in(50,"Table1")
        player3.player_buy_in(50,"Table1")

        player5.player_buy_in(50, "Table2")
        player4.player_buy_in(50, "Table2")

        player3.player_fold()
        # player2.player_check("Table2")
        # player2.player_check("Table2")
        # player2.player_check("Table2")
        # player2.player_check("Table2")
        player1.player_raise(10, "Table1")
        player2.player_raise(10, "Table1")
        player3.player_raise(10, "Table1")
        player4.player_raise(10, "Table2")
        table1.remove_player(player1)

        game1.game_over()
        game2.game_over()


    def generate_class_demo2(self):
        # IF THE GAME ALREADY EXISTS FEED IN BLANK PLAYERS,
        # AND THEY WILL AUTOGENERATE PULLING FIELDS FROM MODELS :D

        table1 = TableClass([], "Table1")
        table1 = TableClass([], "Table1")

        game1 = GameClass([],"Table1")
        game2 = GameClass([],"Table2")


        game1.start()  # gives all players new hands
        game2.start()

        # if the model already exists pass in a blank array it will repopulate players
        for playerX in game1.players_in_game:
            # too broke for another game after the first, so gving them more money to test this again
            playerX.give_player_winnings(100)
            playerX.player_raise(10, "Table1")

        for playerX in game2.players_in_game:
            playerX.player_raise(10, "Table2")

        game2.game_over()
        game1.game_over()


# class Command:
#     message = ''
#
#     def command(self, args):
#
#         userInput = args.split(":")
#         first = userInput[0]
#         second = userInput[1]
#         # if first == "demo":
#         #     self.generate_class_demo()
#
#         if first == "admin":
#
#             checkCommand = second.split(" ")
#             # TODO Admin logging in
#             if checkCommand[0] == "login" and len(checkCommand) == 2:
#                 checkAdmin = checkCommand[1]
#                 admin = Admin.objects.filter(username=checkAdmin)
#                 if not admin:
#                     return f"Admin {checkAdmin} does not exist or invalid admin username"
#                 return f"Admin {checkAdmin} is logging in"
#             # TODO Admin logging out
#             elif checkCommand[0] == "logout":
#                 adminUsername = checkCommand[1]
#                 admin = Admin.objects.filter(username=adminUsername)
#
#                 if not admin:
#                     return f"Admin {adminUsername} does not exist or invalid username"
#                 _admin = Admin.objects.get(username__exact=adminUsername)
#                 _admin.is_online = False
#                 _admin.save()
#                 return f"Admin {adminUsername} is logging out"
#             # TODO Create new Admin [login,cheenou,password,cheenou}
#             elif checkCommand[0] == "login" and len(checkCommand) > 2:
#                 adminUsername = checkCommand[1]
#                 checkAdmin = Admin.objects.filter(username=adminUsername)
#                 if checkAdmin:
#                     return f"Admin already exist"
#
#                 adminPassword = checkCommand[2]
#                 # admin = Admin.objects.create(username=adminUsername, password=adminPassword, is_online=True)
#                 admin = AdminClass(adminUsername, adminPassword, False)
#                 admin.go_online(adminUsername, adminPassword)
#                 return f"Admin {adminUsername} successfully created and online"
#         # TODO checking if username belong to admin before using the commands
#
#         isAdmin = Admin.objects.filter(username=first)
#         if isAdmin:
#             checkCommand = second.split(" ")
#
#             validationAdmin = first
#             adminUsername = Admin.objects.filter(username=validationAdmin)
#
#             if not adminUsername:
#                 return f"username {validationAdmin} is not an admin username"
#
#             getCommand = checkCommand[0]
#
#             getAdminPassword = Admin.objects.get(username__exact=validationAdmin)
#             adminPassword = getAdminPassword.password
#             getPlayerUsername = checkCommand[1]
#             admin = AdminClass(validationAdmin, adminPassword, getAdminPassword.is_online)
#             if not getCommand or len(checkCommand) == 1:
#                 return f"{adminUsername} use one of the admin command to continue"
#             # TODO if admin command is create_player
#             if getCommand == "create_player":
#                 player = Player.objects.filter(username=getPlayerUsername)
#                 if player:
#                     return f"username {getPlayerUsername} already exist"
#                 admin.create_new_player(getPlayerUsername, adminUsername)
#                 return f"username {getPlayerUsername} successfully created"
#
#             # TODO if admin create new table
#             # TODO command use, "(admin username):create_table (table name)" - players must exist
#             if getCommand == "create_table":
#                 tableName = checkCommand[1]
#                 table = Table.objects.filter(tablename=tableName)
#                 if table:
#                     return f"{tableName} already exist, choose different name"
#                 table = Table.objects.create(tablename=tableName)
#                 return f" Table {table} created"
#
#             # TODO add player to table
#             # TODO command used, (admin username):add_to_table (player username) (table name)
#
#             if getCommand == "add_to_table":
#                 checkTable = checkCommand[2]
#                 playerName = checkCommand[1]
#                 table = Table.objects.get(tablename=checkTable)
#
#                 if not table:
#                     return f"{table} does not exist"
#
#                 player = Player.objects.get(username__exact=playerName)
#                 if not player:
#                     return f"username {playerName} does not exist"
#                 playerInTable = table.players.filter(username__exact=playerName)
#                 if playerInTable:
#                     return f"player {playerName} already in table"
#                 table.players.add(player)
#                 return f"{playerName} is added to table"
#
#             # TODO admin start game
#             # TODO, command use, (admin username):start_game (Table name)
#
#             if getCommand == "start_game":
#
#                 theTable = ""
#                 tableName = checkCommand[1]
#                 players = []
#                 getTable = ''
#                 if Table:
#                     for each in Table.objects.all():
#                         getTable = each
#                         break
#                     if getTable.players.all():
#                         for player in getTable.players.all():
#                             players.append(PlayerClass(player.username))
#
#                 if len(checkCommand) == 2:
#
#                     tableName = checkCommand[1]
#                     theTable = Table.objects.get(tablename__exact=tableName)
#                     if not theTable:
#                         return f"invalid table name {theTable}"
#
#                     if theTable.players.count() < 2:
#                         return "Can't start game, need at least 2 players in the table to start game"
#
#                 game = GameClass(players, tableName)
#                 game.start()
#                 new_game = Game.objects.get(tablename=tableName)
#                 if new_game:
#                     new_game.game_live = True
#                     new_game.save()
#
#                 # game.game_over()
#                 return
#
#             if getCommand == "end_game":
#
#                 theTable = ""
#                 tableName = checkCommand[1]
#                 players = []
#                 getTable = ''
#
#                 if Table:
#                     for each in Table.objects.all():
#                         getTable = each
#                         break
#
#                     if getTable.players.all():
#                         for player in getTable.players.all():
#                             players.append(PlayerClass(player.username))
#                         if player.status == "NONE":
#                                 return f"invalid game over {player} has not bet, check, raise, fold"
#
#                 if len(checkCommand) == 2:
#                     tableName = checkCommand[1]
#                     theTable = Table.objects.get(tablename__exact=tableName)
#                     if not theTable:
#                         return f"invalid table name {theTable}"
#
#                 game = GameClass(players, tableName)
#                 new_game = Game.objects.get(tablename=tableName)
#                 if game:
#                     new_game.game_live = False
#                     new_game.save()
#                     game.game_over()
#
#                 return "game over"
#
#
#                 # thisTable = ''
#                 # if Table:
#                 #     for each in Table.objects.all():
#                 #         thisTable = each
#                 #     for each in thisTable.players.all():F
#                 #         each.status = 'FOLD'
#                 #         each.save()
#         players = []
#         getTable = ''
#         if Table:
#             for each in Table.objects.all():
#                 getTable = each
#                 break
#             if getTable.players.all():
#                 for player in getTable.players.all():
#                     players.append(PlayerClass(player.username))
#         myStatus = []
#         countStatus = 0
#         for i in getTable.players.all():
#             myStatus.append(i.status)
#
#         for i in myStatus:
#             if i == "FOLD":
#                 countStatus = countStatus + 1
#
#         if len(players) - countStatus == 1:
#             if Game:
#                 for game in Game.objects.all():
#                     game.game_live = True
#                     game.save()
#
#         # TODO command used, "(player username):(player action, bet, call, check, raise, fold) (chip amount)
#         players = []
#         table = ''
#         if Table:
#             for i in Table.objects.all():
#                 table = i
#
#         for j in table.players.all():
#             if j:
#                 players.append(PlayerClass(j.username))
#         else:
#
#             action = second.split(" ")
#             table = ""
#             for i in Table.objects.all():
#                 table = i
#             c = table.players.filter(username=first)
#
#             if not c:
#                 return f"This player {first} does not exist in the table"
#             status = action[0]
#
#             myGame = GameClass(players, table.tablename)
#             isPlayerBroke = Player.objects.get(username__exact=first)
#
#             # checkStatus = []
#             # count = 0
#             thisTable = ' '
#             for i in Table.objects.all():
#                 thisTable = i
#             myTable = Table.objects.get(tablename__exact=thisTable)
#             #
#             # for i in myTable.players.all():
#             #     checkStatus.append(i.status)
#             # for i in checkStatus:
#             #     if i == "FOLD":
#             #         count = count + 1
#             #
#             # if len(players) - count == 1:
#             #     myGame.game_over()
#
#             if status == "bet":
#
#                 chips = action[1]
#
#                 if isPlayerBroke.chips == 0 or isPlayerBroke.chips < int(chips):
#                     return f"player {first} doesn't have enough chips"
#                 if not chips:
#                     return "please enter amount of chip you want to bet"
#                 myTable.players.filter(username=first)
#
#                 if not myTable:
#                     return f"players {first} is not in the game"
#
#                 player = PlayerClass(first)
#                 Player.objects.filter(username__exact=first).update(status="RAISE")
#                 act = player.player_bet(int(chips), myTable.tablename)
#                 return f"player {first} {status} with {chips} chips"
#
#                 # return
#             # TODO Check command, (player username):check
#             if status == "check":
#                 myTable.players.filter(username=first)
#
#                 if not myTable:
#                     return f"players {first} is not in the game"
#
#                 player = PlayerClass(first)
#                 a = player.player_check(myTable.tablename)
#                 return f"player {first} {status}"
#
#             if status == "fold":
#                 player = myTable.players.filter(username=first)
#
#                 if not player:
#                     return f"players {first} is not in the game"
#
#                 player = PlayerClass(first)
#                 player.player_fold()
#                 myStatus = []
#                 countStatus = 0
#                 for i in getTable.players.all():
#                     myStatus.append(i.status)
#
#                 for i in myStatus:
#                     if i == "FOLD":
#                         countStatus = countStatus + 1
#
#                 if len(players) - countStatus == 1:
#                     if Game:
#                         for game in Game.objects.all():
#                             game.game_live = True
#                             game.save()
#                 return f"player {first} {status}"
#
#             if status == "raise":
#
#                 chips = action[1]
#                 if isPlayerBroke.chips == 0 or isPlayerBroke.chips < int(chips):
#                     return f"player {first} doesn't have enough chips"
#                 if not chips:
#                     return "please enter amount of chip you want to bet"
#                 myTable.players.filter(username=first)
#
#                 if not myTable:
#                     return f"players {first} is not in the game"
#
#                 player = PlayerClass(first)
#
#                 myPlayer = player.player_raise(int(chips), myTable.tablename)
#
#                 Player.objects.filter(username=first).update(status="RAISE")
#                 return f"player {first} {status} with {chips} chips"
#
#             if status == "call":
#
#                 chips = action[1]
#                 if isPlayerBroke.chips == 0 or isPlayerBroke.chips < int(chips):
#                     return f"player {first} doesn't have enough chips"
#                 if not chips:
#                     return "please enter amount of chip you want to bet"
#                 myTable.players.filter(username=first)
#
#                 if not myTable:
#                     return f"players {first} is not in the game"
#
#                 player = PlayerClass(first)
#                 player.player_call(int(chips), myTable.tablename)
#                 Player.objects.filter(username=first).update(status="RAISE")
#                 return f"player {first} {status} with {chips} chips"from AppATDD.models import Player