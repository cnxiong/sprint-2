from AppATDD.models import Admin
from AppATDD.models import Player

class AccountID:

    def __init__(self, username, password, online):
        if username is None or password is None or online is None:
            raise NameError("invalid entry for admin account")
        if not isinstance(username, str) and not isinstance(password, str):
            raise TypeError("Incorrect data type entered, please enter a string")
        if not isinstance(online, bool):
            raise TypeError("Incorrect data type entered for is_online")

        self.username = username
        self._isOnline = online
        self.password = password

        if not Admin.objects.filter(username=self.username).exists():
            Admin.objects.create(username=username,is_online=online,password=password)
        else:
            # IF THE MODEL EXISTS REGENERATE THE PYTHON CLASS FROM THE MODEL VALUES
            self.username = Admin.objects.get(username=username).username
            self.password = Admin.objects.get(username=username).password
            self.password = Admin.objects.get(username=username).is_online

    def go_online(self, admin_login, admin_password):
        try:
            admin = Admin.objects.filter(username__exact=admin_login, password__exact=admin_password).update(
                is_online=True)
        except Admin.DoesNotExist:
            admin = None
            return "Admin Login Unsuccessful"
        except (TypeError, NameError, ValueError, AssertionError):
            return "Admin Login Unsuccessful"
        return "Admin logged in"

    def is_online(self,username):
        # a boolean function to determine whether or not you are online
        return self._isOnline == True

    def reset_password(self, password):
        # check if there is a password,
        if not isinstance(password, str):
            raise TypeError("Incorrect data type entered, please enter a string")
        # re-set password for Admin account
        Admin.objects.filter(username__exact=self.username, password__exact=self.password).update(password=password)
        self.password = password
        return f"New password {self.password} set"


    def set_username(self, username):
        if not isinstance(username, str):
            raise TypeError("Incorrect data type entered, please enter a string")
        # assign new username to the associated account
        Admin.objects.filter(username__exact=self.username).update(username=username)
        self.username = username
        return True

    def check_username(self, username):
        if not isinstance(username, str):
            raise TypeError("Incorrect data type entered, please enter a string")
        if Admin.objects.filter(username__exact=username).exists():
            return f"Admin {username} exists"
        else:
            return f"{username} does not exits"
