from AppATDD.classes.card import CardClass
import random

class DeckClass:

    def __init__(self):
        self.validDeck = self._generateNewDeck()
        self.count = 52
        self.shuffled = False
        # create a standard 52 card deck of cards
        # The logic should be in here to create a deck of cards
        # The deck when first created will be in order of smallest to largest

    def count(self):
        # return the number of cards remaining in the deck
        return self.count

    def draw_in_order(self):
        # return and remove the top card in the deck
        # if the deck is empty, raise a ValueError
        if self.count == 0:
            raise ValueError("Deck is Empty.")
        self.count = self.count - 1
        return self.validDeck.pop(0)

    def draw(self):
        if self.shuffled == False:
            self.shuffle()
            self.shuffled = True
        # return and remove the top card in the deck
        # if the deck is empty, raise a ValueError
        if self.count == 0:
            raise ValueError("Deck is Empty.")
        self.count = self.count - 1
        return self.validDeck.pop(0)

    def shuffle(self):
        # shuffle the ordered array
        random.shuffle(self.validDeck)
        return self.validDeck

    def _generateNewDeck(self):
        # return the number of cards remaining in the deck
        # Deck is an array of Card objects...
        self.validDeck = []
        for Suit in range(1, 5):
            # valid deck creation
            for Rank in range(1, 14):
                card = CardClass(Suit, Rank)
                self.validDeck.append(card)
                # print(card.__str__())

        return self.validDeck

    def __str__(self):
        for card in self.validDeck:
            print(card.__str__())