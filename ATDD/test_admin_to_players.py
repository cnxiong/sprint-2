# this class will test the dealer to see if it would do the following PIBs
# Dealer making a new account, w/ username only
# Dealer adding a player to an existing/available table
# Special user name that identify user name as an admin/dealer

from AppATDD.classes.adminaccount import AdminClass
from AppATDD.classes.player import PlayerClass
from AppATDD.classes.command import Command
from django.test import TestCase


class DealerToPlayerTest(TestCase):
    def setUp(self):
        self.ui = Command()
        self.ui.command("admin:login ADMIN PASSWORD")
        self.ui.command("ADMIN:create_player PLAYER1")
        self.ui.command("ADMIN:create_player PLAYER2")
        self.ui.command("ADMIN:create_table TABLE1")
        self.ui.command("ADMIN:add_to_table PLAYER1 TABLE1")

    # The Following list are our PBIs for dealer:
    # 1. As a dealer, I want to able to create players account with username only
    #   a. so that I can add player to the game
    #          *******Note******
    #          the admin account will be created during signup
    #          with username/password. Once the account is created and login successfully,
    #          They then can create or add player to the table. The maximum players for the
    #          table is 5.
    # 2. As a dealer, I want to have a pre-pended special username [pre-pended
    # on each command line] so that I identified as a dealer
    # 3. As a dealer, I want to be the only one to deal the cards, so the players can't cheat
    # 4. As a player, I want to have the username per-pended so that my moves are identified
    # 5. As a player, I want to have the ability to see current chips held,
    #   a. Amount needed to call/bet/raise
    #   b. the total pot so that I can make a smart decision

    # (PBI's 1) this test will look for the correct/incorrect dealer username
    def test_InvalidDealerUsername(self):
        self.ui.command("admin:login ADMIN PASSWORD")
        self.assertEqual(self.ui.command("admin:login ADMINX"), "Admin ADMINX does not exist or invalid admin username")

    def test_InvalidDealerUsernameType(self):
        self.ui.command("admin:login ADMIN PASSWORD")
        self.assertEqual(self.ui.command("admin:login PASSWORD"),"Admin PASSWORD does not exist or invalid admin username")

    def test_ValidDealer(self):
        self.assertEqual(self.ui.command("admin:login ADMIN PASSWORD"), "Admin already exist")


    # Not in the PBI, but this is to make sure that players username are unique
    def test_playerAlreadyExist(self):
        # I want to be able to create players accounts with username only
        self.ui.command("ADMIN:create_player PLAYER1")
        self.ui.command("ADMIN:create_player PLAYER2")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER1"),'username PLAYER1 already exist')
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER2"), 'username PLAYER2 already exist')


    # 1. As a dealer, I want to able to create players account with username only
    def test_AddNewPlayer(self):
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYERX"),
                         'username PLAYERX successfully created')

    def test_AddNewPlayer(self):
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYERX"),
                         'username PLAYERX successfully created')

    def test_Add2NewPlayers(self):
        self.assertEqual(self.ui.command("ADMIN:create_player playte675"),
                         'username playte675 successfully created')
        self.assertEqual(self.ui.command("ADMIN:create_player alhgflHF"),
                         'username alhgflHF successfully created')

    def test_Add3NewPlayers(self):
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER3"),
                         'username PLAYER3 successfully created')
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER4"),
                         'username PLAYER4 successfully created')
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER5"),
                         'username PLAYER5 successfully created')

    #   a. so that I can add player to the game

    # PBIs # 1. checking for table to see if it reach the max capacity
    def test_TableFull(self):
        players = ["PLAYER3","PLAYER4","PLAYER5"]
        self.ui.command("admin:login ADMIN")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE1"), "TABLE1 already exist, choose different name")

        for player in players:
            input = f"ADMIN:create_player {player}"
            self.ui.command(input)

        for player in players:
            input = f"ADMIN:add_to_table {player} TABLE1"
            output = f"{player} is added to table"
            self.assertEqual(self.ui.command(input),output)

    def test_startNewGame(self):
        self.ui.command("ADMIN:start_game TABLE1")

    # The follow Acceptance tests will look for
    # PBI's:
    # 2. As an admin, I want to have a pre-pended special username [pre-pended
    def test_invalidPrependedUserName(self):
        self.assertEqual(self.ui.command("Xadmin:login ADMINX"), "This player Xadmin does not exist in the table")

    def test_invalid_end_game(self):
        self.assertEqual(self.ui.command("XADMIN:end_game TABLE1 "), "This player XADMIN does not exist in the table")
        self.assertEqual(self.ui.command("ADMIN:end_game TABLE1 "), "invalid game over PLAYER1 has not bet, check, raise, fold")

    # 3. As a dealer, I want to be the only one to deal the cards, so the players can't cheat
    def test_start_game(self):
        self.assertEqual(self.ui.command("ADMIN:start_game TABLE1"), "Can't start game, need at least 2 players in the table to start game")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE2"), " Table TABLE2 created")

    def test_start_game_invalid(self):
        self.assertEqual(self.ui.command("PLAYER1:start_game TABLE2"), None)
        self.assertEqual(self.ui.command("PLAYER1:end_game TABLE1 "), None)