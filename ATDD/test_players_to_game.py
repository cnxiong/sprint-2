from AppATDD.classes.player import PlayerClass
from AppATDD.classes.adminaccount import AdminClass
from django.test import TestCase
from AppATDD.classes.command import Command


class PlayersToGame(TestCase):
    def setUp(self):
        self.ui = Command()

    # Assume the following already created in Django project
    # Model <players>
    # Model <Game>
    # Command line file as ui
    # The following tests only testing for PBI numbers 4 and 5:
    # 4. As a player, I want to have the username per-pended so that my moves are identified
    # 5. As a player, I want to have the ability to see current chips held,
    #   a. Amount needed to call/bet/raise
    #   b. the total pot so that I can make a smart decision

    # these 2 tests are testing for PBI 4
    # As a player, I want to have the username per-pended so that my moves are identified
    def test_invalidPlayerPrependName(self):
        self.assertEqual(self.ui.command("admin:login ADMIN PASSWORD"), "Admin ADMIN successfully created and online")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER1"), "username PLAYER1 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER2"), "username PLAYER2 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER3"), "username PLAYER3 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE1"), " Table TABLE1 created")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER1 TABLE1"), "PLAYER1 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER2 TABLE1"), "PLAYER2 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER3 TABLE1"), "PLAYER3 is added to table")
        self.ui.command("ADMIN:start_game TABLE1")

        self.assertEqual(self.ui.command("XPLAYER1:bet 10"), "This player XPLAYER1 does not exist in the table")
        self.assertEqual(self.ui.command("gPLAYER2:raise 20 "), "This player gPLAYER2 does not exist in the table")
        self.assertEqual(self.ui.command("78PLAYER3:bet 20 "), "This player 78PLAYER3 does not exist in the table")

    def test_validPlayerPrependName(self):
        self.assertEqual(self.ui.command("admin:login ADMIN PASSWORD"), "Admin ADMIN successfully created and online")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER1"), "username PLAYER1 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER2"), "username PLAYER2 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER3"), "username PLAYER3 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE1"), " Table TABLE1 created")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER1 TABLE1"), "PLAYER1 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER2 TABLE1"), "PLAYER2 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER3 TABLE1"), "PLAYER3 is added to table")
        self.ui.command("ADMIN:start_game TABLE1")

        self.assertEqual(self.ui.command("PLAYER1:bet 10"), "player PLAYER1 bet with 10 chips")
        self.assertEqual(self.ui.command("PLAYER2:raise 20 "), "player PLAYER2 raise with 20 chips")
        self.assertEqual(self.ui.command("PLAYER3:bet 20 "), "player PLAYER3 bet with 20 chips")

    # the follow 2 test cases are testing for PBi number 5:
    # As a player, I want to have the ability to see current chips held,
    # TODO this was done by visually diaplaying user data

    # The follow tests will test for PBI's 5 a nd B amount needed to call/bet/raise
    def test_invalidAmountCall(self):
        self.assertEqual(self.ui.command("admin:login ADMIN PASSWORD"), "Admin ADMIN successfully created and online")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER1"), "username PLAYER1 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER2"), "username PLAYER2 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER3"), "username PLAYER3 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE1"), " Table TABLE1 created")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER1 TABLE1"), "PLAYER1 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER2 TABLE1"), "PLAYER2 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER3 TABLE1"), "PLAYER3 is added to table")
        self.ui.command("ADMIN:start_game TABLE1")

        # TODO!! ADDRESS NEGATIVE CHIP INPUT VALUES....
        self.assertEqual(self.ui.command("PLAYER1:call -100"), "player PLAYER1 call with -100 chips")
        self.assertEqual(self.ui.command("PLAYER2:call -10"), "player PLAYER2 call with -10 chips")

        with self.assertRaises(ValueError):
            self.ui.command("PLAYER3:bet 20A")
        with self.assertRaises(ValueError):
            self.ui.command("PLAYER3:call FU")
        with self.assertRaises(ValueError):
            self.ui.command("PLAYER3:bet 20vA")
        with self.assertRaises(ValueError):
            self.ui.command("PLAYER3:call FjkhU")

    # test for amount needed to call

    def test_validAmountCall(self):
        self.assertEqual(self.ui.command("admin:login ADMIN PASSWORD"), "Admin ADMIN successfully created and online")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER1"), "username PLAYER1 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER2"), "username PLAYER2 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER3"), "username PLAYER3 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE1"), " Table TABLE1 created")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER1 TABLE1"), "PLAYER1 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER2 TABLE1"), "PLAYER2 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER3 TABLE1"), "PLAYER3 is added to table")
        self.ui.command("ADMIN:start_game TABLE1")

        self.assertEqual(self.ui.command("PLAYER1:call 10"), "player PLAYER1 call with 10 chips")
        self.assertEqual(self.ui.command("PLAYER2:call 10"), "player PLAYER2 call with 10 chips")
        self.assertEqual(self.ui.command("PLAYER3:call 10"), "player PLAYER3 call with 10 chips")

    # test for amount needed to bet
    def test_invalidAmountBet(self):
        self.assertEqual(self.ui.command("admin:login ADMIN PASSWORD"), "Admin ADMIN successfully created and online")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER1"), "username PLAYER1 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER2"), "username PLAYER2 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER3"), "username PLAYER3 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE1"), " Table TABLE1 created")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER1 TABLE1"), "PLAYER1 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER2 TABLE1"), "PLAYER2 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER3 TABLE1"), "PLAYER3 is added to table")
        self.ui.command("ADMIN:start_game TABLE1")

        # TODO!! ADDRESS NEGATIVE CHIP INPUT VALUES....
        self.assertEqual(self.ui.command("PLAYER1:bet -10"), "player PLAYER1 bet with -10 chips")
        self.assertEqual(self.ui.command("PLAYER2:bet -200 "), "player PLAYER2 bet with -200 chips")

    def test_invalidAmountBetValueError(self):
        # with self.assertRaises(ValueError):
        #     self.ui.command("PLAYER3:bet 100000")
        with self.assertRaises(ValueError):
            self.ui.command("PLAYER3:bet ())hjhhg0A")

    def test_validAmountBet(self):
        self.assertEqual(self.ui.command("admin:login ADMIN PASSWORD"), "Admin ADMIN successfully created and online")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER1"), "username PLAYER1 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER2"), "username PLAYER2 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER3"), "username PLAYER3 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE1"), " Table TABLE1 created")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER1 TABLE1"), "PLAYER1 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER2 TABLE1"), "PLAYER2 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER3 TABLE1"), "PLAYER3 is added to table")
        self.ui.command("ADMIN:start_game TABLE1")

        self.assertEqual(self.ui.command("PLAYER1:bet 10"), "player PLAYER1 bet with 10 chips")
        self.assertEqual(self.ui.command("PLAYER2:bet 20 "), "player PLAYER2 bet with 20 chips")
        self.assertEqual(self.ui.command("PLAYER3:bet 25 "), "player PLAYER3 bet with 25 chips")


    # test for amount needed to Raise
    def test_invalidAmountRaise(self):
        self.assertEqual(self.ui.command("admin:login ADMIN PASSWORD"), "Admin ADMIN successfully created and online")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER1"), "username PLAYER1 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER2"), "username PLAYER2 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER3"), "username PLAYER3 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE1"), " Table TABLE1 created")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER1 TABLE1"), "PLAYER1 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER2 TABLE1"), "PLAYER2 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER3 TABLE1"), "PLAYER3 is added to table")
        self.ui.command("ADMIN:start_game TABLE1")

        self.assertEqual(self.ui.command("PLAYER1:raise 10000"), "player PLAYER1 doesn't have enough chips")
        self.assertEqual(self.ui.command("PLAYER3:raise 99"), "player PLAYER3 doesn't have enough chips")

    def test_invalidAmountRaiseValueError(self):
        with self.assertRaises(ValueError):
            self.ui.command("PLAYER2:raise 10A")
        with self.assertRaises(ValueError):
            self.ui.command("PLAYER2:raise 1y0A")
        with self.assertRaises(ValueError):
            self.ui.command("PLAYER2:raise 1y0A7896")

    def test_validAmountRaise(self):
        self.assertEqual(self.ui.command("admin:login ADMIN PASSWORD"), "Admin ADMIN successfully created and online")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER1"), "username PLAYER1 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER2"), "username PLAYER2 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER3"), "username PLAYER3 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE1"), " Table TABLE1 created")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER1 TABLE1"), "PLAYER1 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER2 TABLE1"), "PLAYER2 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER3 TABLE1"), "PLAYER3 is added to table")
        self.ui.command("ADMIN:start_game TABLE1")

        self.assertEqual(self.ui.command("PLAYER1:raise 10"), "player PLAYER1 raise with 10 chips")
        self.assertEqual(self.ui.command("PLAYER2:raise 15"), "player PLAYER2 raise with 15 chips")
        self.assertEqual(self.ui.command("PLAYER3:raise 12"), "player PLAYER3 raise with 12 chips")


    # Test for total amount pot in the game
    def test_displayInvalidTotalPot(self):
        # self.ui.command("Login Chee password500")
        # self.ui.command("Dealer:login")
        # self.ui.command("Dealer:Create_player ThisCheese")
        # self.ui.command("ThisCheese: created with 500 chips")
        # self.ui.command("Dealer:Create_player BlueCheese")
        # self.ui.command("BlueCheese: created with 910 chips")
        # self.ui.command("Dealer:Start_game")
        # self.ui.command("Dealer:Dealing")
        # self.ui.command("BlueCheese with 0: All-in (910)")
        # self.ui.command("ThisCheese with 0: All-In (500)")
        # self.assertEqual(self.ui.command("Total Pot (10,000)"), "Incorrect total pot <10,000>")
        pass  # TODO currently visually displayed, no textual return statement

    def test_displayValidTotalPot(self):
        # self.ui.command("Login Chee password500")
        # self.ui.command("Dealer:login")
        # self.ui.command("Dealer:Create_player ThisCheese")
        # self.ui.command("ThisCheese: created with 500 chips")
        # self.ui.command("Dealer:Create_player BlueCheese")
        # self.ui.command("BlueCheese: created with 910 chips")
        # self.ui.command("Dealer:Start_game")
        # self.ui.command("Dealer:Dealing")
        # self.ui.command("(BlueCheese with 0: All-in (910)")
        # self.ui.command("(Player) ThisCheese with 0: All-In (500)")
        # self.assertEqual(self.ui.command("Total Pot (1410)"), "Total pot <1,410>")
        pass #TODO currently visually displayed, no textual return statement


    # Dealer will first deal the cards to the players after the dealer creates the accounts for them.
    # So, player cannot deal the cards among themselves

    # Since only a dealer can make and restart new game, this acceptance test is to catch any
    # error if the players were to made a new game
    def test_playerMakeNewGame(self):
        self.assertEqual(self.ui.command("admin:login ADMIN PASSWORD"), "Admin ADMIN successfully created and online")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER1"), "username PLAYER1 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER2"), "username PLAYER2 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_player PLAYER3"), "username PLAYER3 successfully created")
        self.assertEqual(self.ui.command("ADMIN:create_table TABLE1"), " Table TABLE1 created")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER1 TABLE1"), "PLAYER1 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER2 TABLE1"), "PLAYER2 is added to table")
        self.assertEqual(self.ui.command("ADMIN:add_to_table PLAYER3 TABLE1"), "PLAYER3 is added to table")

        self.assertEqual(self.ui.command("PLAYER1:start_game TABLE1"), None)
        self.assertEqual(self.ui.command("PLAYER2:start_game TABLE1"), None)
        self.assertEqual(self.ui.command("PLAYER3:start_game TABLE1"), None)


    # Assume the following already created in Django project
    # Model <players>
    # ThisCheese hand(card1(1,10),card2(4,3),card3(2,1),card4(5,9),card5(2,2))
    # (10 of Clubs, 3 of Hearts, Ace of Spades, invalid, 2 of Spades)
    # BlueCheese hand(card1(1,5),card2(2,3),card3(2,11),card4(4,9),card5(2,15))
    # (5 of Clubs, 3 of Spades, Jack of Spades, 9 of Diamonds, invalid)
    # CheeseCurbs hand(card1(1,5),card2(2,3),card3(2,11),card4(4,9),card5(2,13))
    # (5 of Clubs, 3 of Spades, Jack of Spades, 9 of Diamonds, King of Spades)
    # HANDS ARE VISIBLE IN THE PLAYER LOGS AND DO NOT RETURN PRINT STATEMENTS